# WDM CockroachDB

This project demonstrates the use of microservices with CockroachDB. There are three services, each of which contains a Python Django web server. All three services connect to the same CockroachDB cluster but utilise their own database within the cluster.

The microservices communicate via asynchronous message publishing and subscribing, where messages are sent through RabbitMQ. 
 
Additionally, a SAGA is implemented in the checkout system with RPC to improve consistency.

## Requirements

You must have a machine with docker running and docker-compose to run this project.

## Installation:

Clone the project, and enter the project directory.

To install run: `./install.sh`

On the first run, you need to create the tables on CockroachDB:

Run `./start.sh`

Wait for the stack to start (10-15 seconds), then run:

```
docker-compose exec node_1 ./cockroach sql --insecure --execute="CREATE DATABASE orders;CREATE DATABASE payments;CREATE DATABASE stock;"
```

## Usage

To run the development version, run `./start.sh`

To run the production version, run `./start-prod.sh`

If you need to refresh the database (clear all rows in the tables), run `./refresh.sh`
