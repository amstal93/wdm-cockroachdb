#!/bin/bash
docker-compose exec node_1 ./cockroach sql --insecure --execute="CREATE DATABASE IF NOT EXISTS orders;CREATE DATABASE IF NOT EXISTS payments;CREATE DATABASE IF NOT EXISTS stock;"

docker-compose exec stock_service python manage.py flush --no-input
docker-compose exec order_service python manage.py makemigrations
docker-compose exec order_service python manage.py migrate

docker-compose exec stock_service python manage.py flush --no-input
docker-compose exec payment_service python manage.py makemigrations
docker-compose exec payment_service python manage.py migrate

docker-compose exec stock_service python manage.py flush --no-input
docker-compose exec stock_service python manage.py makemigrations
docker-compose exec stock_service python manage.py migrate
