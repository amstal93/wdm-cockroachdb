#!/bin/sh

echo "Waiting for payment service..."
while ! nc -z $PAYMENT_SERVICE_HOST $PAYMENT_SERVICE_PORT; do
  sleep 5
done
echo "Payment service started"

exec "$@"
