from django.urls import path

from .views import OrderViewSet

urlpatterns = [
    path(
        'create/<int:user_id>', OrderViewSet.as_view(
            {
                'post': 'create'
            }
        )
        ),
    path(
        'remove/<int:order_id>', OrderViewSet.as_view(
            {
                'delete': 'remove',
            }
        )
        ),
    path(
        'find/<int:order_id>', OrderViewSet.as_view(
            {
                'get': 'find',
            }
        )
        ),
    path(
        'addItem/<int:order_id>/<int:item_id>', OrderViewSet.as_view(
            {
                'post': 'add_item',
            }
        )
        ),
    path(
        'removeItem/<int:order_id>/<int:item_id>', OrderViewSet.as_view(
            {
                'delete': 'remove_item',
            }
        )
        ),
    path(
        'checkout/<int:order_id>', OrderViewSet.as_view(
            {
                'post': 'checkout',
            }
        )
        ),
]
